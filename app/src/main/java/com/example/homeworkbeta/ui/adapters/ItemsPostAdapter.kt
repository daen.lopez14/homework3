package com.example.homeworkbeta.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.NavOptionsBuilder
import androidx.navigation.navOptions
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.homeworkbeta.data.model.ItemPost
import com.example.homeworkbeta.databinding.ItemPostBinding

class ItemsPostAdapter : ListAdapter<ItemPost, ItemsPostAdapter.ItemPostsViewHolder>(DiffCallback) {

    class ItemPostsViewHolder(
        private var binding: ItemPostBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.setClickListener {
                binding.itemPost?.let {
                    navOptions {
                        NavOptionsBuilder()
                    }
                }
            }
        }

        fun bind(post: ItemPost) {
            binding.itemPost = post
            // This is important, because it forces the data binding to execute immediately,
            // which allows the RecyclerView to make the correct view size measurements
            binding.executePendingBindings()
        }
    }

    companion object DiffCallback : DiffUtil.ItemCallback<ItemPost>() {
        override fun areItemsTheSame(oldItem: ItemPost, newItem: ItemPost): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: ItemPost, newItem: ItemPost): Boolean {
            return oldItem.title == newItem.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemPostsViewHolder {
        return ItemPostsViewHolder(
            ItemPostBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: ItemPostsViewHolder, position: Int) {
        val post = getItem(position)
        holder.bind(post)
    }
}