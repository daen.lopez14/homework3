package com.example.homework3.viewmodel

import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.homeworkbeta.data.model.ItemPost
import com.example.homeworkbeta.ui.adapters.ItemsPostAdapter

@BindingAdapter("postTitle")
fun bindTitle(textView: TextView, title: String) {
    textView.text = title
}

@BindingAdapter("listData")
fun bindRecyclerView(
    recyclerView: RecyclerView,
    data: List<ItemPost>?
) {
    val adapter = recyclerView.adapter as ItemsPostAdapter
    adapter.submitList(data)


}