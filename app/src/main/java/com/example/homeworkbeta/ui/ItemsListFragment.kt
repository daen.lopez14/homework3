package com.example.homeworkbeta.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.homeworkbeta.databinding.FragmentItemsListBinding
import com.example.homeworkbeta.ui.adapters.ItemsPostAdapter
import com.example.homeworkbeta.viewmodel.MainViewModel

class ItemsListFragment : Fragment() {

    /**
     * infla el layout con DataBinding, sets its lifecycle owner al ItemsListFragment para habilitar DataBinding para observar
     * LiveData, and sets up the RecyclerView with an adapter
     */

    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding = FragmentItemsListBinding.inflate(inflater)

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.postList.adapter = ItemsPostAdapter()

        return binding.root

        //return inflater.inflate(R.layout.fragment_items_list, container, false)
    }

}