package com.example.homeworkbeta.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

data class ItemPost(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String
)

//Room
@Entity(tableName = "posts_table")
data class ItemPostEntity(
    @ColumnInfo(name = "userId")
    val userId: Int,
    @PrimaryKey @ColumnInfo(name = "id")
    val id: Int,
    @ColumnInfo(name = "title")
    val title: String,
    @ColumnInfo(name = "body")
    val body: String
)


