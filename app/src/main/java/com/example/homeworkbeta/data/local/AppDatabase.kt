package com.example.homeworkbeta.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.homeworkbeta.data.model.CommentsEntity
import com.example.homeworkbeta.data.model.ItemPostEntity

@Database(entities = [ItemPostEntity::class, CommentsEntity::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    //aqui ya se crea la instancia de room con el metodo getdatabase
    //principal punto de entrada para que la app interactue con la DDBB

    abstract fun postsDao(): PostsDao

    //our database will only have one instance of its class
    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            //if the INSTANCE is not null, then return it,
            //if it is, then create the database
            return INSTANCE ?: synchronized(this){
                //synchronized: everything within that block will be protected from a concurrent execution
                //by a multiple thread
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "posts_table").build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}

/*INSTANCE = INSTANCE ?: Room.databaseBuilder(
               context.applicationContext,
               AppDatabase::class.java,
               "Posts_table"
           ).build()
           return INSTANCE*/