package com.example.homeworkbeta.data.remote

import com.example.homeworkbeta.repository.WebService
import com.example.homeworkbeta.data.model.ItemPost

class RemoteDataSource(private val webService: WebService) {

    suspend fun getPosts(): List<ItemPost> = webService.getPosts()

    //el id tendra que variar de acuerdo al post que yo toque. ese item debe de mandar un id para ejecutar este comando

    //suspend fun getCommentsByPostId(): List<Comments> = webService.getCommentsByPostId()
}