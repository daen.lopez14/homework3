package com.example.homework3.data.Repository.repository

import com.example.homeworkbeta.data.model.ItemPost
import com.example.homeworkbeta.data.remote.RemoteDataSource
import com.example.homeworkbeta.repository.ItemPostRepository


/**
 * va al datasource a buscar informacion. crear una instancia del datasource para poder acceder a los metodos
 **/
class Repository(
    private val dataSourceRemote: RemoteDataSource,
) : ItemPostRepository {

    override suspend fun getPosts(): List<ItemPost> = dataSourceRemote.getPosts()
    /*override suspend fun getPosts(): List<ItemPost> {
        dataSourceRemote.getPosts().forEach {
            localDataSource.saveDataSet()
        }
    }*/

    //override suspend fun getDataSet() : LiveData<List<ItemPostEntity>> = localDataSource.getDataSet()

}

//override suspend fun getCommentsByPostId(): List<Comments> = dataSource.getCommentsByPostId(0)


//IMPLEMENTAR CORUTINAS

/*class Repository(private val postsDao: PostsDao){

suspend fun getPosts() = postsDao.getPosts()
//suspend fun getCommentsByPostId(id: Int) = postsDao.getCommentsByPostId()
}*/