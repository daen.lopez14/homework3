package com.example.homeworkbeta.repository

import com.example.homeworkbeta.data.model.ItemPost

interface ItemPostRepository {

    suspend fun getPosts(): List<ItemPost>

    //suspend fun getCommentsByPostId() : List<Comments>
}