package com.example.homeworkbeta.repository

import com.example.homeworkbeta.data.model.ItemPost
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

private const val BASE_URL =
    "https://jsonplaceholder.typicode.com/"

private val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

interface WebService {

    //establece una conexión de red con el servidor, se comunica con el servidor y recibe
    //una respuesta JSON. Utilizarás un servidor backend que ya está escrito

    //encargado de usar el retrofit para traer la informacion del servidor
    //este webservice va a ir al servidor a buscar la informacion de los metodos
    //transforma la informacion gracias a retrofit

    @GET("posts")
    suspend fun getPosts(): List<ItemPost>

    /*@GET("comments")
    suspend fun getCommentsByPostId(@Query("postId") id: Int): List<PostComment>*/
    /*@GET("comments")
    suspend fun getCommentsByPostId(@Query("postId") id: Int): List<Comments>*/
}

object RetrofitClient {
    //se va inicar solamente cuando llame a webservice no antes
    //retrofit es la instancia que me va proporcionar la base url, el convertidor y esa instancia con el webservice
    val webService: WebService by lazy {
        retrofit.create(WebService::class.java)
    }
}
