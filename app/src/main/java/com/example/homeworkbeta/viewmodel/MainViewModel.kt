package com.example.homeworkbeta.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.homeworkbeta.data.model.ItemPost
import com.example.homeworkbeta.repository.RetrofitClient
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    //Crear MutableLiveEvent. En el mainFragment se tiene que observar.
    //El adapter debe recibir un MutableLiveEvent. Cuando se crea la instancia se le pasa un MutableLiveEvent
    //desde el fragement se estara observando. EN postAdapter cuando se cree el vm del adapter recibira
    //ya con el MTLE en el vm de adapter, se puede accedeer a este mtle desde el adapter. ONCLICK

    // The internal MutableLiveData that stores the status of the most recent request

    private val _status = MutableLiveData<String>()
    private val _posts = MutableLiveData<List<ItemPost>>()

    // The external immutable LiveData for the request status
    val status: MutableLiveData<String> = _status
    val posts: MutableLiveData<List<ItemPost>> = _posts

    init {
        getItemsPosts()
    }

    //Gets Mars photos information from the Mars API Retrofit service and updates the
    //[MarsPhoto] [List] [LiveData].
    private fun getItemsPosts() {
        //Un ViewModelScope es el alcance integrado de corrutinas definido para cada ViewModel en tu app.
        //Usarás ViewModelScope para iniciar la corrutina y realizar la llamada de red Retrofit en segundo plano.
        viewModelScope.launch {
            try {
                //_posts.value = getItemsPosts()
                _posts.value = RetrofitClient.webService.getPosts()
                _status.value = "retrieve successful"
            } catch (e: Exception) {
                _status.value = "Failure: ${e.message}"
            }
        }
    }

}